package test

import (
	"fmt"
	"gitee.com/juliangip/juliang-go-sdk/com.juliang"
	"gitee.com/juliangip/juliang-go-sdk/com.juliang/common"
	"testing"
)

const (
	dynamicTradeNo  = "your dynamic trade no"
	dynamicTradeKey = "your dynamic key"

	userId  = "you user id"
	userKey = "you user key"

	aloneTradeNo = "you alone trade no"
	aloneKey     = "you alone key"

	postPayTradeNo = "you post pay trade no"
	postPayKey     = "you post pay key"

	unlimitedTradeNo = "you unlimited trade no"
	unlimitedKey     = "you unlimited key"

	companyPostpayTradeNo = "your company postpay trade no"
	companyPostpayKey     = "your company postpay key"

	companyDynamicTradeNo = "your company dynamic trade no"
	companyDynamicKey     = "your company dynamic key"
)

// 不限量 -- 提取ip
func TestUnlimitedGetIps(t *testing.T) {
	var unlimitedGetIps common.UnlimitedGetIps
	unlimitedGetIps.TradeNo = unlimitedTradeNo
	unlimitedGetIps.Key = unlimitedKey
	unlimitedGetIps.Num = "1"
	fmt.Println(juliang.UnlimitedGetIps(unlimitedGetIps))
}

// 不限量 -- 设置白名单
func TestUnlimitedSetWhiteIp(t *testing.T) {
	var unlimitedSetWhiteIp common.UnlimitedSetWhiteIp
	unlimitedSetWhiteIp.TradeNo = unlimitedTradeNo
	unlimitedSetWhiteIp.Key = unlimitedKey
	unlimitedSetWhiteIp.Ips = "223.6.6.6,223.5.5.5"
	fmt.Println(juliang.UnlimitedSetWhiteIp(unlimitedSetWhiteIp))
}

// 不限量 -- 获取白名单
func TestUnlimitedGetWhiteIp(t *testing.T) {
	var unlimitedGetWhiteIp common.UnlimitedGetWhiteIp
	unlimitedGetWhiteIp.TradeNo = unlimitedTradeNo
	unlimitedGetWhiteIp.Key = unlimitedKey
	fmt.Println(juliang.UnlimitedGetWhiteIp(unlimitedGetWhiteIp))
}

// 不限量 -- 替换白名单
func TestUnlimitedReplaceWhiteIp(t *testing.T) {
	var unlimitedReplaceWhiteIp common.UnlimitedReplaceWhiteIp
	unlimitedReplaceWhiteIp.TradeNo = unlimitedTradeNo
	unlimitedReplaceWhiteIp.Key = unlimitedKey
	unlimitedReplaceWhiteIp.NewIp = "223.6.6.6"
	unlimitedReplaceWhiteIp.OldIp = "223.6.6.6"
	fmt.Println(juliang.UnlimitedReplaceWhiteIp(unlimitedReplaceWhiteIp))
}

// 账户 -- 获取账户余额
func TestLanguage_usersGetBalance(t *testing.T) {
	var balance common.UsersGetBalance
	balance.SetUser_id(userId)
	balance.SetKey(userKey)
	value := juliang.UsersGetBalance(balance)
	fmt.Println(value)
}

// 获取账户下对应产品类型的所有正常订单信息
func TestLanguage_usersGetAllOrders(t *testing.T) {
	var gao common.UsersGetAllOrders
	gao.SetKey(userKey)
	gao.SetUser_id(userId)
	gao.SetProduct_type("1")
	gao.SetAttach_key("1")
	value := juliang.UsersgetAllOrders(gao)
	fmt.Println(value)
}

// 获取所属省份的可用代理城市信息
func Test_GetCityInfo(t *testing.T) {
	var cityRequest common.UsersGetCity
	cityRequest.SetKey(userKey)
	cityRequest.SetUser_id(userId)
	cityRequest.SetProvince("湖北,河北,山东")
	value := juliang.UsersGetCity(cityRequest)
	fmt.Print(value)
}

// 替换Ip白名单
func TestLanguage_aloneReplaceWhiteIp(t *testing.T) {
	var whiteIp common.AloneReplaceWhiteIp
	whiteIp.SetTrade_no(aloneTradeNo)
	whiteIp.SetKey(aloneKey)
	whiteIp.SetOld_ip("171.42.101.123")
	whiteIp.SetNew_ip("223.6.6.6")
	value := juliang.AloneReplaceWhiteIp(whiteIp)
	fmt.Println(value)
}

// 独享代理 -- 获取代理IP白名单
func TestLanguage_aloneGetWhiteIp(t *testing.T) {
	var whiteIp common.AloneGetWhiteIp
	whiteIp.SetTrade_no(aloneTradeNo)
	whiteIp.SetKey(aloneKey)
	value := juliang.AloneGetWhiteIp(whiteIp)
	fmt.Println(value)
}

// 独享代理 -- 设置代理IP白名单
func TestLanguage_aloneSetWhiteIp(t *testing.T) {
	var whiteIp common.AloneSetWhiteIp
	whiteIp.SetTrade_no(aloneTradeNo)
	whiteIp.SetKey(aloneKey)
	whiteIp.SetIps("171.40.164.78")
	value := juliang.AloneSetWhiteIp(whiteIp)
	fmt.Println(value)
}

// 独享代理 -- 获取独享代理详情
func TestLanguage_aloneGetIps(t *testing.T) {
	var ips common.AloneGetIps
	ips.SetTrade_no(aloneTradeNo)
	ips.SetKey(aloneKey)
	value := juliang.AloneGetIps(ips)
	fmt.Println(value)
}

// 动态代理 -- 获取剩余可提取IP数量
func TestLanguage_dynamicBalance(t *testing.T) {
	var balance common.DynamicBalance
	balance.SetKey(dynamicTradeKey)
	balance.SetTrade_no(dynamicTradeNo)
	value := juliang.DynamicBalance(balance)
	fmt.Println(value)
}

// 动态代理 -- 获取代理剩余可用时长
func TestLanguage_dynamicRemain(t *testing.T) {
	var remain common.DynamicRemain
	remain.SetKey(dynamicTradeKey)
	remain.SetTrade_no(dynamicTradeNo)
	remain.SetProxy("1.1.1.1:8088")
	value := juliang.DynamicRemain(remain)
	fmt.Println(value)
}

// 动态代理 -- 获取代理IP白名单
func TestLanguage_dynamicGetWhiteIp(t *testing.T) {
	var getWhiteIp common.DynamicGetWhiteIp
	getWhiteIp.SetKey(dynamicTradeKey)
	getWhiteIp.SetTrade_no(dynamicTradeNo)
	value := juliang.DynamicGetWhiteIp(getWhiteIp)
	fmt.Println(value)
}

// 动态代理 -- 替换Ip白名单
func TestLanguage_dynamicReplaceWhiteIp(t *testing.T) {
	var ip common.DynamicReplaceWhiteIp
	ip.SetTrade_no(dynamicTradeNo)
	ip.SetKey(dynamicTradeKey)
	//ip.SetOld_ip("10.10.10.10")
	ip.SetNew_ip("223.6.6.6,255.255.255.255")
	ip.SetReset("1")
	value := juliang.DynamicReplaceWhiteIp(ip)
	fmt.Println(value)
}

// 动态代理 -- 设置代理IP白名单
func TestLanguage_dynamicSetWhiteIp(t *testing.T) {
	var setWhiteIp common.DynamicSetWhiteIp
	setWhiteIp.SetKey(dynamicTradeKey)
	setWhiteIp.SetTrade_no(dynamicTradeNo)
	setWhiteIp.SetIps("171.40.164.78")
	value := juliang.DynamicSetWhiteIp(setWhiteIp)
	fmt.Println(value)
}

// 动态代理 -- 校验代理有效性
func TestLanguage_dynamicCheck(t *testing.T) {
	var dynamicCheck common.DynamicCheck
	dynamicCheck.SetKey(dynamicTradeKey)
	dynamicCheck.SetTrade_no(dynamicTradeNo)
	dynamicCheck.SetProxy("58.219.244.199:50154")
	value := juliang.DynamicCheck(dynamicCheck)
	fmt.Println(value)
}

// 动态代理 -- 提取动态代理
func TestLanguage_dynamicGetIps(t *testing.T) {
	var dynamicGetIps common.DynamicGetIps
	dynamicGetIps.SetKey(dynamicTradeKey)
	dynamicGetIps.SetTrade_no(dynamicTradeNo)
	dynamicGetIps.SetNum("10")
	dynamicGetIps.SetPt("1")
	dynamicGetIps.SetResult_type("json")
	dynamicGetIps.SetSplit("4")
	value := juliang.DynamicGetIps(dynamicGetIps)
	fmt.Println(value)
}

// 包量代理（企业版） -- 获取ip池
func TestLanguage_companyDynamicGetIps(t *testing.T) {
	var companyDynamic common.CompanyDynamic
	companyDynamic.Key = companyDynamicKey
	companyDynamic.TradeNo = companyDynamicTradeNo
	companyDynamic.Num = "1"
	value := juliang.CompanyDynamicGetIps(companyDynamic)
	fmt.Println(value)
}

// 包量代理（企业版） -- 获取ip白名单
func TestLanguage_CompanyDynamicGetWhiteIp(t *testing.T) {
	var companyDynamicGetWhiteIp common.CompanyDynamicGetWhiteIp
	companyDynamicGetWhiteIp.Key = companyDynamicKey
	companyDynamicGetWhiteIp.TradeNo = companyDynamicTradeNo
	value := juliang.CompanyDynamicGetWhiteIp(companyDynamicGetWhiteIp)
	fmt.Println(value)
}

// 包量代理（企业版） -- 设置白名单
func TestLanguage_CompanyDynamicSetWhiteIp(t *testing.T) {
	var companyDynamicSetWhiteIp common.CompanyDynamicSetWhiteIp
	companyDynamicSetWhiteIp.Key = companyDynamicKey
	companyDynamicSetWhiteIp.TradeNo = companyDynamicTradeNo
	companyDynamicSetWhiteIp.Ips = "171.40.167.185"
	value := juliang.CompanyDynamicSetWhiteIp(companyDynamicSetWhiteIp)
	fmt.Println(value)
}

// 包量代理（企业版）-- 删除IP白名单
func TestLanguage_CompanyDynamicDelWhiteIp(t *testing.T) {
	var companyDynamicDelWhiteIp common.CompanyDynamicDelWhiteIp
	companyDynamicDelWhiteIp.Key = companyDynamicKey
	companyDynamicDelWhiteIp.TradeNo = companyDynamicTradeNo
	companyDynamicDelWhiteIp.DelIp = "171.40.167.185"
	value := juliang.CompanyDynamicDelWhiteIp(companyDynamicDelWhiteIp)
	fmt.Println(value)
}

// 按量付费 -- 提取ip
func TestLanguage_postPayGetIps(t *testing.T) {
	var postPayGetIps common.PostPayGetIps
	postPayGetIps.Key = postPayKey
	postPayGetIps.TradeNo = postPayTradeNo
	postPayGetIps.Num = "10"
	value := juliang.PostPayGetIps(postPayGetIps)
	fmt.Println(value)
}

// 按量付费 -- 检查ip可用性
func TestLanguage_postPayCheckIps(t *testing.T) {
	var postPayCheckIps common.PostPayCheckIps
	postPayCheckIps.Key = postPayKey
	postPayCheckIps.TradeNo = postPayTradeNo
	postPayCheckIps.Proxy = "60.20.200.119:53778"
	value := juliang.PostPayCheckIps(postPayCheckIps)
	fmt.Println(value)
}

// 按量付费 -- 设置白名单
func TestLanguage_postPaySetWhiteIp(t *testing.T) {
	var postPaySetWhiteIp common.PostPaySetWhiteIp
	postPaySetWhiteIp.Key = postPayKey
	postPaySetWhiteIp.TradeNo = postPayTradeNo
	postPaySetWhiteIp.Ips = "223.6.6.6"
	value := juliang.PostPaySetWhiteIp(postPaySetWhiteIp)
	fmt.Println(value)
}

// 按量付费 -- 获取ip白名单
func TestLanguage_postPayGetWhiteIp(t *testing.T) {
	var postPayGetWhiteIp common.PostPayGetWhiteIp
	postPayGetWhiteIp.Key = postPayKey
	postPayGetWhiteIp.TradeNo = postPayTradeNo
	value := juliang.PostPayGetWhiteIp(postPayGetWhiteIp)
	fmt.Println(value)
}

// 按量付费 -- 替换IP白名单
func TestLanguage_postPayReplaceWhiteIp(t *testing.T) {
	var postPayReplaceWhiteIp common.PostPayReplaceWhiteIp
	postPayReplaceWhiteIp.Key = postPayKey
	postPayReplaceWhiteIp.TradeNo = postPayTradeNo
	postPayReplaceWhiteIp.NewIp = "223.6.6.6"
	postPayReplaceWhiteIp.Reset = "1"
	value := juliang.PostPayReplaceWhiteIp(postPayReplaceWhiteIp)
	fmt.Println(value)
}

// 按量付费（企业版） -- 提取ip
func TestLanguage_CompanyPostPayGetIps(t *testing.T) {
	var companyPostPayGetIps common.CompanyPostPayGetIps
	companyPostPayGetIps.Key = companyPostpayKey
	companyPostPayGetIps.TradeNo = companyPostpayTradeNo
	companyPostPayGetIps.Num = "1"
	value := juliang.CompanyPostPayGetIps(companyPostPayGetIps)
	fmt.Println(value)
}

// 按量付费（企业版） -- 获取ip白名单
func TestLanguage_CompanyPostPayGetWhiteIp(t *testing.T) {
	var companyPostPayGetWhiteIp common.CompanyPostPayGetWhiteIp
	companyPostPayGetWhiteIp.Key = companyPostpayKey
	companyPostPayGetWhiteIp.TradeNo = companyPostpayTradeNo
	value := juliang.CompanyPostPayGetWhiteIp(companyPostPayGetWhiteIp)
	fmt.Println(value)
}

// 按量付费（企业版） -- 设置白名单
func TestLanguage_CompanyPostPaySetWhiteIp(t *testing.T) {
	var companyPostPaySetWhiteIp common.CompanyPostPaySetWhiteIp
	companyPostPaySetWhiteIp.Key = companyPostpayKey
	companyPostPaySetWhiteIp.TradeNo = companyPostpayTradeNo
	companyPostPaySetWhiteIp.Ips = "171.40.164.116"
	value := juliang.CompanyPostPaySetWhiteIp(companyPostPaySetWhiteIp)
	fmt.Println(value)
}

// 按量付费（企业版）-- 替换IP白名单
func TestLanguage_CompanyPostPayDelWhiteIp(t *testing.T) {
	var companyPostPayDelWhiteIp common.CompanyPostPayDelWhiteIp
	companyPostPayDelWhiteIp.Key = companyPostpayKey
	companyPostPayDelWhiteIp.TradeNo = companyPostpayTradeNo
	companyPostPayDelWhiteIp.DelIp = "171.40.164.116"
	value := juliang.CompanyPostPayDelWhiteIp(companyPostPayDelWhiteIp)
	fmt.Println(value)
}
