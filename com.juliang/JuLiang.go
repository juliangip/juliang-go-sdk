package juliang

import (
	"gitee.com/juliangip/juliang-go-sdk/com.juliang/common"
	"gitee.com/juliangip/juliang-go-sdk/com.juliang/enums"
	"gitee.com/juliangip/juliang-go-sdk/com.juliang/ext"
	"net/url"
	"reflect"
)

// UsersGetBalance 获取账户余额
func UsersGetBalance(balance common.UsersGetBalance) string {
	t := reflect.TypeOf(balance)
	v := reflect.ValueOf(balance)
	params := make(map[string]string)
	appKey := balance.Key()
	params = ext.StructToMap(t, v, params)
	getipsParams := ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UsersGetBalance)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// UsersgetAllOrders 获取账户下对应类型的所有正常状态订单号
func UsersgetAllOrders(orders common.UsersGetAllOrders) string {
	t := reflect.TypeOf(orders)
	v := reflect.ValueOf(orders)
	params := make(map[string]string)
	appKey := orders.Key()
	params = ext.StructToMap(t, v, params)
	getipsParams := ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UsersGetAllOrders)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// UsersGetCity 获取所属省份可用代理城市信息
func UsersGetCity(cityInfo common.UsersGetCity) string {
	t := reflect.TypeOf(cityInfo)
	v := reflect.ValueOf(cityInfo)
	params := make(map[string]string)
	appKey := cityInfo.Key()
	params = ext.StructToMap(t, v, params)
	getCityParams := ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UsersGetCity)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getCityParams)
}

// DynamicGetIps 动态代理 -- 提取动态代理
func DynamicGetIps(dynamicgetips common.DynamicGetIps) string {
	t := reflect.TypeOf(dynamicgetips)
	v := reflect.ValueOf(dynamicgetips)
	params := make(map[string]string)
	appKey := dynamicgetips.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicGetIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// DynamicCheck 动态代理 -- 校验IP可用性
func DynamicCheck(check common.DynamicCheck) string {
	t := reflect.TypeOf(check)
	v := reflect.ValueOf(check)
	params := make(map[string]string)
	appKey := check.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicCheck)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// DynamicSetWhiteIp 动态代理 -- 设置代理IP白名单
func DynamicSetWhiteIp(ip common.DynamicSetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicSetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// DynamicReplaceWhiteIp 动态代理 -- 替换IP白名单
func DynamicReplaceWhiteIp(ip common.DynamicReplaceWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicReplaceWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// DynamicGetWhiteIp 动态代理 -- 获取IP白名单
func DynamicGetWhiteIp(ip common.DynamicGetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicGetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// DynamicRemain 动态代理 -- 获取代理剩余可用时长
func DynamicRemain(remain common.DynamicRemain) string {
	t := reflect.TypeOf(remain)
	v := reflect.ValueOf(remain)
	params := make(map[string]string)
	appKey := remain.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicRemain)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// DynamicBalance 动态代理 -- 获取剩余可用时长
func DynamicBalance(balance common.DynamicBalance) string {
	t := reflect.TypeOf(balance)
	v := reflect.ValueOf(balance)
	params := make(map[string]string)
	appKey := balance.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.DynamicBalance)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// CompanyDynamicGetIps 包量代理 -- 提取动态代理
func CompanyDynamicGetIps(ips common.CompanyDynamic) string {
	t := reflect.TypeOf(ips)
	v := reflect.ValueOf(ips)
	params := make(map[string]string)
	appKey := ips.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyDynamicGetIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyDynamicGetWhiteIp 包量代理(企业版) -- 获取代理IP白名单
func CompanyDynamicGetWhiteIp(ip common.CompanyDynamicGetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyDynamicGetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyDynamicSetWhiteIp  包量代理(企业版) -- 设置代理IP白名单
func CompanyDynamicSetWhiteIp(ip common.CompanyDynamicSetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyDynamicSetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyDynamicDelWhiteIp  包量代理 -- 替换IP白名单
func CompanyDynamicDelWhiteIp(ip common.CompanyDynamicDelWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyDynamicDelWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// AloneGetIps 独享代理 -- 获取代理详情
func AloneGetIps(ips common.AloneGetIps) string {
	t := reflect.TypeOf(ips)
	v := reflect.ValueOf(ips)
	params := make(map[string]string)
	appKey := ips.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.AloneGetIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// AloneSetWhiteIp 独享代理 -- 设置代理IP白名单
func AloneSetWhiteIp(ip common.AloneSetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.AloneSetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// AloneReplaceWhiteIp 独享代理 -- 替换IP白名单
func AloneReplaceWhiteIp(ip common.AloneReplaceWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.AloneReplaceWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// AloneGetWhiteIp 独享代理 -- 获取代理IP白名单
func AloneGetWhiteIp(ip common.AloneGetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key()
	params = ext.StructToMap(t, v, params)
	var getipsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.AloneGetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getipsParams)
}

// PostPayGetIps 按量付费 -- 获取代理详情
func PostPayGetIps(ips common.PostPayGetIps) string {
	t := reflect.TypeOf(ips)
	v := reflect.ValueOf(ips)
	params := make(map[string]string)
	appKey := ips.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.PostPayGetIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// PostPayCheckIps 按量付费 -- 检查ip的可用性
func PostPayCheckIps(ips common.PostPayCheckIps) string {
	t := reflect.TypeOf(ips)
	v := reflect.ValueOf(ips)
	params := make(map[string]string)
	appKey := ips.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.PostPayCheckIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// PostPaySetWhiteIp 按量付费 -- 设置代理IP白名单
func PostPaySetWhiteIp(ip common.PostPaySetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.PostPaySetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// PostPayGetWhiteIp 按量付费 -- 获取代理IP白名单
func PostPayGetWhiteIp(ip common.PostPayGetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.PostPayGetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// PostPayReplaceWhiteIp 按量付费 -- 替换IP白名单
func PostPayReplaceWhiteIp(ip common.PostPayReplaceWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.PostPayReplaceWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyPostPayGetIps 按量付费(企业版) -- 获取代理详情
func CompanyPostPayGetIps(ips common.CompanyPostPayGetIps) string {
	t := reflect.TypeOf(ips)
	v := reflect.ValueOf(ips)
	params := make(map[string]string)
	appKey := ips.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyPostPayGetIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyPostPayGetWhiteIp 按量付费(企业版) -- 获取代理IP白名单
func CompanyPostPayGetWhiteIp(ip common.CompanyPostPayGetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyPostPayGetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyPostPaySetWhiteIp 按量付费(企业版) -- 设置代理IP白名单
func CompanyPostPaySetWhiteIp(ip common.CompanyPostPaySetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyPostPaySetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// CompanyPostPayDelWhiteIp 按量付费 -- 替换IP白名单
func CompanyPostPayDelWhiteIp(ip common.CompanyPostPayDelWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.CompanyPostPayDelWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// UnlimitedGetIps 不限量 -- 提取ip
func UnlimitedGetIps(ip common.UnlimitedGetIps) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UnlimitedGetIps)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// UnlimitedSetWhiteIp 不限量 -- 设置代理IP白名单
func UnlimitedSetWhiteIp(ip common.UnlimitedSetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UnlimitedSetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// UnlimitedGetWhiteIp 不限量 -- 获取代理IP白名单
func UnlimitedGetWhiteIp(ip common.UnlimitedGetWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UnlimitedGetWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}

// UnlimitedReplaceWhiteIp 不限量 -- 替换IP白名单
func UnlimitedReplaceWhiteIp(ip common.UnlimitedReplaceWhiteIp) string {
	t := reflect.TypeOf(ip)
	v := reflect.ValueOf(ip)
	params := make(map[string]string)
	appKey := ip.Key
	params = ext.StructTagToMap(t, v, params)
	var getIpsParams = ext.GetParams(params, appKey)
	Url, err := url.Parse(enums.UnlimitedReplaceWhiteIp)
	if err != nil {
		return ""
	}
	return ext.Post(Url, getIpsParams)
}
