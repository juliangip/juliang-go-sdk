package enums

const (
	// DOMAIN 主站地址
	DOMAIN = "http://v2.api.juliangip.com"
	// UsersGetBalance
	//DOMAIN = "http://192.168.10.80:8199"
	//USERS_GETBALANCE 获取账户余额
	UsersGetBalance = DOMAIN + "/users/getbalance"
	// UsersGetAllOrders 获取账户下对应类型的所有正常状态订单信息
	UsersGetAllOrders = DOMAIN + "/users/getAllOrders"
	//USERS_GETCITY	获取对应省份可用代理城市信息
	UsersGetCity = DOMAIN + "/users/getCity"

	// DynamicGetIps 动态代理 -- 提取动态代理
	DynamicGetIps = DOMAIN + "/dynamic/getips"
	// DynamicCheck 动态代理 -- 校验IP可用性
	DynamicCheck = DOMAIN + "/dynamic/check"
	// DynamicSetWhiteIp 动态代理 -- 设置代理IP白名单
	DynamicSetWhiteIp = DOMAIN + "/dynamic/setwhiteip"
	// DynamicReplaceWhiteIp 动态代理 -- 替换IP白名单
	DynamicReplaceWhiteIp = DOMAIN + "/dynamic/replaceWhiteIp"
	// DynamicGetWhiteIp 动态代理 -- 获取IP白名单
	DynamicGetWhiteIp = DOMAIN + "/dynamic/getwhiteip"
	// DynamicRemain 动态代理 -- 获取代理剩余可用时长
	DynamicRemain = DOMAIN + "/dynamic/remain"
	// DynamicBalance 动态代理 -- 获取剩余可用时长
	DynamicBalance = DOMAIN + "/dynamic/balance"

	// CompanyDynamicGetIps 动态代理企业版
	CompanyDynamicGetIps = DOMAIN + "/company/dynamic/getips"
	// CompanyDynamicSetWhiteIp CompanyDynamicGetIps 动态代理企业版
	CompanyDynamicSetWhiteIp = DOMAIN + "/company/dynamic/setwhiteip"
	// CompanyDynamicGetWhiteIp CompanyDynamicGetIps 动态代理企业版
	CompanyDynamicGetWhiteIp = DOMAIN + "/company/dynamic/getwhiteip"
	// CompanyDynamicDelWhiteIp CompanyDynamicGetIps 动态代理企业版
	CompanyDynamicDelWhiteIp = DOMAIN + "/company/dynamic/delwhiteip"

	// AloneGetIps 独享代理 -- 获取代理详情
	AloneGetIps = DOMAIN + "/alone/getips"
	// AloneSetWhiteIp 独享代理 -- 设置代理IP白名单
	AloneSetWhiteIp = DOMAIN + "/alone/setwhiteip"
	// AloneGetWhiteIp 独享代理 -- 获取代理IP白名单
	AloneGetWhiteIp = DOMAIN + "/alone/getwhiteip"
	// AloneReplaceWhiteIp 独享代理 -- 替换IP白名单
	AloneReplaceWhiteIp = DOMAIN + "/alone/replaceWhiteIp"
	// PostPayGetIps 按量付费 -- 获取代理详情
	PostPayGetIps = DOMAIN + "/postpay/getips"
	// PostPayCheckIps 按量付费 -- 校验IP可用性
	PostPayCheckIps = DOMAIN + "/postpay/check"
	// PostPaySetWhiteIp 按量付费 -- 设置代理IP白名单
	PostPaySetWhiteIp = DOMAIN + "/postpay/setwhiteip"
	// PostPayGetWhiteIp 按量付费 -- 获取代理IP白名单
	PostPayGetWhiteIp = DOMAIN + "/postpay/getwhiteip"
	// PostPayReplaceWhiteIp 按量付费 -- 替换IP白名单
	PostPayReplaceWhiteIp = DOMAIN + "/postpay/replaceWhiteIp"

	// CompanyPostPayGetIps 按量付费(企业版) -- 获取代理详情
	CompanyPostPayGetIps = DOMAIN + "/company/postpay/getips"
	// CompanyPostPaySetWhiteIp 按量付费(企业版) -- 添加白名单
	CompanyPostPaySetWhiteIp = DOMAIN + "/company/postpay/setwhiteip"
	// CompanyPostPayGetWhiteIp 按量付费(企业版) -- 获取白名单
	CompanyPostPayGetWhiteIp = DOMAIN + "/company/postpay/getwhiteip"
	// CompanyPostPayDelWhiteIp 按量付费(企业版) -- 删除白名单
	CompanyPostPayDelWhiteIp = DOMAIN + "/company/postpay/delwhiteip"

	// UnlimitedGetIps 不限量 -- 获取代理详情
	UnlimitedGetIps = DOMAIN + "/unlimited/getips"
	// UnlimitedSetWhiteIp 不限量 -- 设置代理IP白名单
	UnlimitedSetWhiteIp = DOMAIN + "/unlimited/setwhiteip"
	// UnlimitedGetWhiteIp 不限量 -- 获取白名单
	UnlimitedGetWhiteIp = DOMAIN + "/unlimited/getwhiteip"
	// UnlimitedReplaceWhiteIp 不限量 -- 替换白名单
	UnlimitedReplaceWhiteIp = DOMAIN + "/unlimited/replaceWhiteIp"
)
