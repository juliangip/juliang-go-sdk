// Package common @Author W
// @Date 2024/4/24
// @desc
package common

// CompanyPostPayGetIps 提取动态代理
type CompanyPostPayGetIps struct {
	TradeNo    string `json:"trade_no"`    //业务编号
	Num        string `json:"num"`         //提取数量
	Pt         string `json:"pt"`          //代理类型
	ResultType string `json:"result_type"` //返回类型
	Split      string `json:"split"`       //结果分隔符
	AuthType   string `json:"auth_type"`   // 认证类型
	Province   string `json:"province"`    //省份  只能填写一个省份
	City       string `json:"city"`        //城市  只能填写一个城市（省份和城市不匹配时，以城市为主）
	IpRemain   string `json:"ip_remain"`   //剩余数量
	Filter     string `json:"filter"`      //去重
	Key        string `json:"key"`         //密钥
}

// CompanyPostPaySetWhiteIp 设置白名单
type CompanyPostPaySetWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	Ips     string `json:"ips"`      //IPs
	Key     string `json:"key"`      //密钥
}

// CompanyPostPayGetWhiteIp 获取白名单
type CompanyPostPayGetWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	Key     string `json:"key"`      //密钥
}

// CompanyPostPayDelWhiteIp 删除白名单
type CompanyPostPayDelWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	DelIp   string `json:"del_ip"`   //新IP
	Key     string `json:"key"`      //密钥
}
