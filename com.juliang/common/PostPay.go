package common

// PostPayGetIps 提取动态代理
type PostPayGetIps struct {
	TradeNo    string `json:"trade_no"`    //业务编号
	Num        string `json:"num"`         //提取数量
	Pt         string `json:"pt"`          //代理类型
	ResultType string `json:"result_type"` //返回类型
	Split      string `json:"split"`       //结果分隔符
	CityName   string `json:"city_name"`   //城市名称
	CityCode   string `json:"city_code"`   //城市编码
	IpRemain   string `json:"ip_remain"`   //剩余数量
	Area       string `json:"area"`        //地区
	NoArea     string `json:"no_area"`     //排除地区
	IpSeg      string `json:"ip_seg"`      //ip段
	NoIpSeg    string `json:"no_ip_seg"`   //排除ip段
	Isp        string `json:"isp"`         //运营商
	Filter     string `json:"filter"`      //去重
	Key        string `json:"key"`         //密钥
}

// PostPayCheckIps 检测代理IP
type PostPayCheckIps struct {
	TradeNo string `json:"trade_no"` //业务编号
	Proxy   string `json:"proxy"`    //代理IP，多个英文逗号分割
	Key     string `json:"key"`      //密钥
}

// PostPaySetWhiteIp 设置白名单
type PostPaySetWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	Ips     string `json:"ips"`      //IPs
	Key     string `json:"key"`      //密钥
}

// PostPayGetWhiteIp 获取白名单
type PostPayGetWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	Key     string `json:"key"`      //密钥
}

// PostPayReplaceWhiteIp 替换白名单
type PostPayReplaceWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	NewIp   string `json:"new_ip"`   //新IP
	OldIp   string `json:"old_ip"`   //旧IP
	Reset   string `json:"reset"`    //是否重置
	Key     string `json:"key"`      //密钥
}
