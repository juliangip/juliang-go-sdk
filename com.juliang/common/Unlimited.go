package common

// UnlimitedGetIps 不限量 -- 提取ip
type UnlimitedGetIps struct {
	TradeNo    string `json:"trade_no"`    //业务编号
	Num        string `json:"num"`         //提取数量
	Pt         string `json:"pt"`          //代理类型
	ResultType string `json:"result_type"` //返回类型
	Split      string `json:"split"`       //结果分隔符
	CityName   string `json:"city_name"`   //城市名称
	CityCode   string `json:"city_code"`   //城市编码
	IpRemain   string `json:"ip_remain"`   //剩余数量
	Key        string `json:"key"`         //密钥
}

// UnlimitedSetWhiteIp 不限量 -- 设置白名单
type UnlimitedSetWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	Ips     string `json:"ips"`      //IPs
	Key     string `json:"key"`      //密钥
}

// UnlimitedGetWhiteIp 不限量 -- 获取白名单
type UnlimitedGetWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	Key     string `json:"key"`      //密钥
}

// UnlimitedReplaceWhiteIp 不限量 -- 替换白名单
type UnlimitedReplaceWhiteIp struct {
	TradeNo string `json:"trade_no"` //业务编号
	NewIp   string `json:"new_ip"`   //新IP
	OldIp   string `json:"old_ip"`   //旧IP
	Reset   string `json:"reset"`    //是否重置
	Key     string `json:"key"`      //密钥
}
